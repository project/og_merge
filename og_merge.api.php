<?php

/**
 * Implementation of hook_og_merge()
 * 
 * This in invoked directly prior to merging one group with another
 * 
 * @param $primary
 *   The group which we are merging items into
 * @param $secondary
 *   The group which we are pulling items from
 * @param $keep_admins
 *   TRUE if we intend to persist the admin status of members from the
 *   secondary group into the primary group
 * @return
 *   Optionally return an array of Batch operations to be included in
 *   the batch process used to merge the groups. You may simply execute 
 *   needed functions inside this hook, but if you have a large or variable
 *   amount of operations, it's highly recommended you return them to be
 *   included in the batch.
 */
function hook_og_merge($primary, $secondary, $keep_admins = FALSE) {
  // Example returning operations for the batch process
  $operations = array();
  $sql = "SELECT * FROM {my_table} WHERE group_nid = %d";
  $results = db_query($sql, $secondary->nid);
  while ($result db_fetch_object($results)) {
    $operations[] = array('custom_merge_callback', array($result->arg1, $result->arg2));
  }
  return $operations;
}
