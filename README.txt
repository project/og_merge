SUMMARY
============================
The purpose of this module is to provide a simple, extensible way to 
merge one organic group with another.

Organic groups comes with a built-in merging mechanism that is 
unsatisfactory. Here is a list of things that OG Merge does better:

- Process the merging operations via a batch operation to avoid a timeout
- Allow user's that do not have the 'administer nodes' permission to perform merges
 * Regular users can only merge groups which they are an admin of
- Provide a hook (hook_og_merge()) to allow other modules to act on the merge
- Allow the perserving of group admin status for users coming from other groups
- Provide a simple form that stands out as a merge feature
 * OG's built-in merge is only present when attempting to delete a group
- Avoid merged content being flagged as 'updated'
 

USAGE
============================
- Install the module as usual - this module has no configuration.
- After intalled, a link will be present inside the Group details block to merge groups
- Regular users with the permission 'merge own groups' can merge any groups which they are an admin of
- Users with the permission 'merge all groups' or 'administer organic groups' can merge any groups
- During a merge, all content and memberships from the selected group will be moved into the target group
- After the merge, the group being merged into the other will be deleted
- An option is present to specify whether or not group admins should remain as admins in the merged group


API
============================
See og_merge.api.php
